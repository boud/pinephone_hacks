Bug status
==========
List of PinePhone bugs posted by or of special interest to Boud and their status that were open as of 2023-10-31.

(C) 2023 Boud GPL-3+

| Package | Start date | Status | Description | Notes |
| :----:  | :--:     | :--: | :------        | :--- |
| [alsa-ucm-conf/Mob 8](https://salsa.debian.org/Mobian-team/packages/alsa-ucm-conf/-/issues/8) | 2023-07-31 | <span style="color:green">closed</span> | PP audio calls do not work out-of-the-box on Mobian/trixie | [hardcore VoiceCall.conf hack](https://codeberg.org/boud/pinephone_hacks/src/branch/main/audio_hacks/Allwinner_A64_PinePhone/VoiceCall.conf) |
| [alsa-ucm-conf/upstr 351](https://github.com/alsa-project/alsa-ucm-conf/issues/351) | 2023-09-12 | <span style="color:green">closed</span> |  AIF2 ADC Stereo Capture Route is uninitialised in alsa-ucm-conf | |
| [alsa-ucm-conf/upstr 366](https://github.com/alsa-project/alsa-ucm-conf/pull/366/files) | 2023-11-03 | <span style="color:#aa0099">rejected</span> |  Documentation: Add ucm URL | |
| [feedbackd/Deb 1050601](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1050601) | 2023-08-26 |  <span style="color:green">closed</span> | feedbackd: package documentation files are not installed into /usr/share/doc/ | |
| [feedbackd/upstr 73](https://source.puri.sm/Librem5/feedbackd/-/issues/73) | 2023-09-03 |  <span style="color:red">open</span> | User effectively locked out of phosh due to feedbackd customisation error/ Try harder to load a theme | |
| [feedback/upstr 76](https://source.puri.sm/Librem5/feedbackd/-/issues/76) | 2023-09-21 | <span style="color:green">closed</span> | fbcli should clearly warn the user that a non-existent event is not recognised | |
| [gnome-calls/upstr 605](https://gitlab.gnome.org/GNOME/calls/-/issues/605) | 2023-10-22 |  <span style="color:red">open – workarounds</span> | gnome-calls does not disconnect the modem USB if the ringing event is non-audio | workarounds to known USB disconnection bug |
| [gnome-settings-daemon/upstr 338](https://gitlab.gnome.org/GNOME/gnome-settings-daemon/-/merge_requests/338) | 2023-09-23 |  <span style="color:green">closed</span> |  power-manager: Show sleep warning conditional to chassis type |  | follows from [phosh 983](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/983) and [pinephone-support/Mobian 20](https://salsa.debian.org/Mobian-team/devices/pinephone-support/-/issues/20) | 
| [gnome-snapshot/upstr 237](https://gitlab.gnome.org/GNOME/snapshot/-/issues/237) | 2024-10-05 | <span style="color:#aa0099">rejected</span> | gnome-snapshot needs a man page |  |
| [gnome-snapshot/Deb 1084158](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1084158) | 2024-10-05 | <span style="color:#aa0099">rejected</span> | man page for gnome-snapshot is missing | |
| [gnome-snapshot/upstr 240](https://gitlab.gnome.org/GNOME/snapshot/-/issues/240) | 2024-10-11 | <span style="color:green">closed</span> | Mobian/trixie pinephone rear camera is black and does not take photos: snapshot 47.0.1-5 |  |
| [iio-sensor-proxy/upstr 380](https://gitlab.freedesktop.org/hadess/iio-sensor-proxy/-/issues/380) | 2023-10-03 | <span style="color:red">open</span> | monitor-sensor needs a man page |  |
| [iio-sensor-proxy/Deb 1053381](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1053381) | 2023-10-03 | <span style="color:red">open</span> | man pages for iio-sensor-proxy and monitor-sensor are missing | |
| [kernel/Mob 90](https://salsa.debian.org/Mobian-team/devices/kernels/sunxi64-linux/-/issues/90) | 2023-09-22 |  <span style="color:green">closed</span> | ima devfreq record idle+0xbc/0xd0 | |
| [mobian-server/Mob 5](https://salsa.debian.org/Mobian-team/mobian-server/-/issues/5) | 2023-11-27 | <span style="color:green">closed</span> | Upstream version numbers are older than Debian and Mobian version numbers | |
| [mobile-broadband-provider-info/MR 116](https://gitlab.gnome.org/GNOME/mobile-broadband-provider-info/-/merge_requests/116) | 2024-10-21 | <span style="color:red">open</span> | pl: update Virgin Mobile | |
| [mmsd/upstr 101](https://gitlab.com/kop316/mmsd/-/issues/101) | 2024-10-20 | <span style="color:red">open</span> | Failure to decode received MMS |  |
| [phosh/upstr 1069](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/1069) | 2024-05-30 | <span style="color:green">closed</span> | Phosh crash when connecting to monitor over HDMI through docking bar | solved within 1 day |
| [pinephone_modem_sdk/upstr 224](https://github.com/the-modem-distro/pinephone_modem_sdk/pull/224) | 2023-11-05 | <span style="color:green">merged</span> | Fix boot-mdm9607.img name; add AT+ADBON info; mmcli update | |
| [pinephone_modem_sdk/upstr 226](https://github.com/the-modem-distro/pinephone_modem_sdk/pull/226) | 2023-11-06 | <span style="color:green">merged</span> | Add link to incoming-call USB disconnections; update UCM2 paths | |
| [pinephone-support/Mob 27](https://salsa.debian.org/Mobian-team/devices/pinephone-support/-/issues/27) | 2023-12-25 | <span style="color:green">archived</span> | OG PinePhone microphone failed but recovered after booting to SD then eMMC | [shimming the U101](https://wiki.pine64.org/wiki/User:Earboxer/Shim_U101) [(archive)](https://archive.today/2024.10.13-204656/https://wiki.pine64.org/wiki/User:Earboxer/Shim_U101) [fixed my mic](https://wiki.pine64.org/wiki/PinePhone_Hardware_Issues#Internal_Microphone) [(archive)](https://archive.today/2024.10.13-204824/https://wiki.pine64.org/wiki/PinePhone_Hardware_Issues) |
| [squeekboard/upstr 375](https://gitlab.gnome.org/World/Phosh/squeekboard/-/issues/375) | 2023-08-29 |  <span style="color:green">closed</span> | Copy/paste popup does not appear when clicking menu button in squeekboard on foot or kgx | |
| [sunxi64-linux/Mob 91](https://salsa.debian.org/Mobian-team/devices/kernels/sunxi64-linux/-/issues/91) | 2023-11-04 | <span style="color:red">open</span> | PinePhone proximity near level should be set to around 70, not 15 | patch merged |
