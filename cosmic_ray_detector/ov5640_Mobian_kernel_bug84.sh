#!/bin/bash
# ov5640_Mobian_kernel_bug84.sh - reproduce bug 84
# (C) 2024 Boud Roukema GPL-3+
# upstream: https://codeberg.org/boud/pinephone_hacks
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

# 2024-11-06: DISCLAIMER: This minimal working example aims to
# reproduce bug #84
# https://salsa.debian.org/Mobian-team/devices/kernels/sunxi64-linux/-/issues/84
# but it is in reality an extract from a longer (not yet published)
# script (coming soon ;)) and has not (yet) been tested directly in
# this particular form.

# The script initially uses 'media-ctl' to find the rear camera and
# check media and video IDs. It then de-selects the front camera and
# selects the rear camera, and sets the video4linux format options.  A
# loop repeatedly takes snapshots with 'ffmpeg'. The loop should take
# valid images for the first 10 minutes to an hour or so and should
# then fail repeatedly.

# Source: https://codeberg.org/boud/pinephone_hacks/cosmic_ray_detector/ 

TMP_DIR=/tmp/bug84/
mkdir -p ${TMP_DIR}

N_PHOTOS=2000

# Find the media device

MEDIA_ID=0
if (media-ctl -d /dev/media${MEDIA_ID} -p |grep -E "gc2145|ov5640"); then
    printf "Found cameras on /dev/media${MEDIA_ID}.\n"
else
    MEDIA_ID=1
    if (media-ctl -d /dev/media${MEDIA_ID} -p |grep -E "gc2145|ov5640"); then
        printf "Found cameras on /dev/media${MEDIA_ID}.\n"
    else
        printf "Cameras not found.\n"
        exit 1
    fi
fi

DEV_VIDEO_ID=$(media-ctl -d /dev/media${MEDIA_ID} --print-topology |grep -o "/dev/video.")
printf "The video device appears to be ${DEV_VIDEO_ID}.\n"

# This script is only for the rear camera.
CAM=rear

# Select the rear or front camera.
# See https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)#Cameras
# https://pine64.org/documentation/PinePhone/Camera/
if [ "x${CAM}" = "xrear" ]; then
    media-ctl -d /dev/media${MEDIA_ID} --links '"gc2145 0-003c":0->"sun6i-csi-bridge":0[0]'
    media-ctl -d /dev/media${MEDIA_ID} --links '"ov5640 0-004c":0->"sun6i-csi-bridge":0[1]'

    # Set formatting options.
    media-ctl -d /dev/media${MEDIA_ID} --set-v4l2 '"ov5640 0-004c":0[fmt:UYVY8_2X8/1920x1080]'

fi

for ((i=0;i<${N_PHOTOS};i++)); do
    # You can probably change from .fits to .jpg and still trigger the bug:
    TMP_FITS=$(mktemp ${TMP_DIR}/img_XXXXXXXX.fits)

    # To view the header of a FITS file:
    # fold -w 80 ${TMP_FITS} | less

    if [ "x${CAM}" = "xrear" ]; then
        ffmpeg -s 1920x1080 -f video4linux2 -i ${DEV_VIDEO_ID} -frames:v 1 -y ${TMP_FITS}
    fi
    sleep 2 # brief delay

    rm -fv ${TMP_FITS} # comment out if you want to look at the images
done
