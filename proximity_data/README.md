proximity_raw_collect_data
==========================

(C) 2023 Boud Roukema GPL-3+

The aim of encouraging people to run this script is to get a sample
(crowdsourced) of pinephone proximity_raw values in order to decide
if a default value can be proposed to the community.

Run `proximity_raw_collect_data` by first browsing the script to check what
you think it does, and after you've checked, do
````
./proximity_raw_collect_data &
````
as an ordinary user (root access is *not* required).

With the default parameters, this should run for at least, and
probably much longer than, two days, outputting time-shuffled data (to
reduce privacy risks) every 2.5 hours of non-suspend time to
`${HOME}/tmp_proximity_raw_data`. The two-day minimum will occur if
you keep your phone unsuspended for 48 hours. The job is an ordinary
user-level process - if the 'suspend' system works normally,
the `proximity_raw_collect_data` script will stop during suspension,
and continue after unsuspension, as if nothing happened during
the period during which the phone was suspended.

Please (if you agree to releasing your data under PD = CC0) provide
your `tmp_proximity_raw_data` N-day data file as a merge request after
forking https://codeberg.org/boud/pinephone_hacks and renaming your file
to `proximity_raw_data.NNN` where `NNN` is a number not yet used in this
directory (or post it in an 'issue' to
https://codeberg.org/boud/pinephone_hacks/issues , since these files
are plain text and small). Thanks! You may add your name or any other
info to the comments section, of course (for example, if you normally
use your phone with a pine64 keyboard, which puts your fingers close
to the sensor and will give high 'raw' values more often).

You can kill the process and give a shorter file than the default full
file (600 non-comment lines) if you are impatient :), or reduce the
INTERVAL (in seconds) to generate a longer data file. Again, if you
modify the parameters from the default, then it would be best to
briefly describe the changes in the # comments section.




Data anonymity
--------------

- The script does *not* do telemetry. If you agree to provide your
  data file, then you need to post a merge request at Codeberg
  (or post a fork or the file anywhere reasonable and request
  a merge).

- By default, time stamps are *not* written to the data file (edit
  the script if you want them - this is easy to find).

- The data are time-shuffled data for every 2.5 hour block of
  non-suspend time. Without access to knowing when your phone was
  suspended (the modem normally remains active), reconstructing
  the history of your hand/ear/body movements near the phone in order
  to match your data file to a particular IMEI identified phone
  (phone calls with your ear at the earpiece yield high
  proximity_raw values) would be difficult, especially if you file
  a merge request a day or so later.

- If you are a high-priority target for surveillance, feel free to do
  an extra run of your full set of data lines through 'shuf' before
  posting the file as a merge request. The only extractable "private"
  data will be what fraction of non-suspend time your (or someone
  else's or some other warm-blooded animal's) ear or hand or paw or
  other body part was near the sensor.


Analysis assumption
-------------------

- The likely analysis assumption for the data is that only a
  small fraction of non-suspend time is during phone calls with
  your ear to the earpiece. If most of your non-suspend time is
  making phone calls with your ear to the earpiece, then please
  add a brief note to help in analysis.

- This is for an exploratory study only, with very few conditions
  controlled, aiming at practical pinephone calibration, not
  a peer-reviewed publication.


Overall plots
-------------

<!--
Wait for https://codeberg.org/forgejo/forgejo/issues/1067 to be implemented.
![Proximity sensor behaviour for PinePhone v1.2 000](../outputs/prox_proximity_raw_data.000.svg "Proximity sensor stk3310 behaviour of PinePhone v1.2 Sep 2023 000")
![Proximity sensor behaviour for PinePhone v1.2 001](../outputs/prox_proximity_raw_data.001.svg "Proximity sensor stk3310 behaviour of PinePhone v1.2 Sep 2023 001")
![Proximity sensor behaviour for PinePhone v1.2 002](../outputs/prox_proximity_raw_data.002.svg "Proximity sensor stk3310 behaviour of PinePhone v1.2 Sep 2023 002")
-->
<a href="../outputs/prox_proximity_raw_data.000.svg"><img src="../outputs/prox_proximity_raw_data.000.svg" title="Proximity sensor stk3310 behaviour of PinePhone v1.2 Sep 2023: file 000" alt="Proximity sensor behaviour for PinePhone v1.2: file 000" width="300" /></a>
<a href="../outputs/prox_proximity_raw_data.001.svg"><img src="../outputs/prox_proximity_raw_data.001.svg" title="Proximity sensor stk3310 behaviour of PinePhone v1.2 Sep 2023: file 001" alt="Proximity sensor behaviour for PinePhone v1.2: file 001" width="300" /></a>
<a href="../outputs/prox_proximity_raw_data.002.svg"><img src="../outputs/prox_proximity_raw_data.002.svg" title="Proximity sensor stk3310 behaviour of PinePhone v1.2 Sep 2023: file 002" alt="Proximity sensor behaviour for PinePhone v1.2: file 002" width="300" /></a>
<a href="../outputs/prox_proximity_raw_data.003.svg"><img src="../outputs/prox_proximity_raw_data.003.svg" title="Proximity sensor stk3310 behaviour of PinePhone-Pro Oct 2023: file 003" alt="Proximity sensor behaviour for PinePhone-Pro file 003" width="300" /></a>
<a href="../outputs/prox_proximity_raw_data.004.svg"><img src="../outputs/prox_proximity_raw_data.004.svg" title="Proximity sensor stk3310 behaviour of PinePhone v1.2a Aug 2020: file 004" alt="Proximity sensor behaviour for PinePhone v1.2a: file 004" width="300" /></a>
<a href="../outputs/prox_proximity_raw_data.005.svg"><img src="../outputs/prox_proximity_raw_data.005.svg" title="Proximity sensor stk3310 behaviour of PinePhone v1.2b Feb 2021: file 004" alt="Proximity sensor behaviour for PinePhone v1.2b: file 005" width="300" /></a>

Histograms
----------

<a href="../outputs/PP_proximity_raw.svg"><img src="../outputs/PP_proximity_raw.svg" title="Proximity_raw sensor stk3310 distribution of PinePhone v1.2 Sep 2023" alt="Proximity_raw distribution for PinePhone v1.2" width="300" /></a>
<a href="../outputs/PPP_proximity_raw.svg"><img src="../outputs/PPP_proximity_raw.svg" title="Proximity_raw sensor stk3310 distribution of PinePhone v1.2 Oct 2023" alt="Proximity_raw distribution for PinePhone v1.2" width="300" /></a>


Copyright
---------

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This script is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
Public License for more details. See <http://www.gnu.org/licenses/>.
