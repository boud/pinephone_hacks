##  histogs - Plot histograms for the PinePhone v1.2 (and PPP) proximity sensor
##  (C) 2023 Boud Roukema GPL-3+
##  upstream: https://codeberg.org/boud/pinephone_hacks
## 
##  This script is free software: you can redistribute it and/or modify it
##  under the terms of the GNU General Public License as published by the
##  Free Software Foundation, either version 3 of the License, or (at your
##  option) any later version.
## 
##  This script is distributed in the hope that it will be useful, but
##  WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
##  Public License for more details. See <http://www.gnu.org/licenses/>.

## To run this file with octave 7.3.0:
## 
## 1. Create the data.* files:
##    for ((i=0;i<6;i++)) ; do grep -v ^# proximity_raw_data.00${i} |awk '{print $1}' > data.${i}; done
##
## 2. Run octave:
##    octave histogs.m
##

1; # dummy line for convenience in emacs octave mode


#To PinePhone or not to PinePhone, that is the question.
for PP=0:1

  if(PP) # PinePhone
    d0 = load('data.0');
    d1 = load('data.1');
    d2 = load('data.2');
    d4 = load('data.4');
    d5 = load('data.5');
  else # PinePhonePro
    d3 = load('data.3');
  endif

                                # Create histogram data
  prox_min=3;
  prox_max=2000;
  N_bins=20
  prox_centres = linspace(log10(prox_min),log10(prox_max),N_bins);
  if(PP)
    [N0 xx] = hist(log10(d0),prox_centres); # N0 = counts per bin for data set 0
    [N1 xx] = hist(log10(d1),prox_centres); # N1 for data set 1 and so on ...
    [N2 xx] = hist(log10(d2),prox_centres); #
    [N4 xx] = hist(log10(d4),prox_centres); #
    [N5 xx] = hist(log10(d5),prox_centres); #
  else
    [N3 xx] = hist(log10(d3),prox_centres); #
  endif

  if(PP)
    counts = [N0'/length(d0) N1'/length(d1) N2'/length(d2) N4'/length(d4) N5'/length(d5)];
  else
    counts = [N3'/length(d3)];  
  endif

  count_offset = 3.5
  bar(prox_centres',log10(counts)+count_offset)

  ylabel(sprintf("log10(counts) + %.1f",count_offset))
  xlabel("proximity\\_raw")
  if(PP)
    title("PinePhone proximity\\_raw distribution")
  else
    title("PinePhonePro proximity\\_raw distribution")
  endif

  max_digit=6
  for i=1:max_digit
    xlogtick(i) = log10(i);
    xlogticklabel{i} = sprintf("%1d",i);
    
    xlogtick(i+max_digit) = log10(i)+1;
    if(i<4)
      xlogticklabel{i+max_digit} = sprintf("%1d",i*10);
    else
      xlogticklabel{i+max_digit} = sprintf("%1d",i);
    endif

    xlogtick(i+2*max_digit) = log10(i)+2;
    if(i<3)
      xlogticklabel{i+2*max_digit} = sprintf("%1d",i*100);
    else
      xlogticklabel{i+2*max_digit} = sprintf("%1d",i);
    endif

    if(i<3)
      xlogtick(i+3*max_digit) = log10(i)+3;
      if(i<3)
        xlogticklabel{i+3*max_digit} = sprintf("%1d",i*1000);
      else
        xlogticklabel{i+3*max_digit} = sprintf("%1d",i);
      endif
    endif
    
  endfor
  set(gca,'xtick',[xlogtick])
  set(gca,'xticklabel',[xlogticklabel])

  if(PP)
    print('PP_proximity_raw.svg','-dsvg')
  else
    print('PPP_proximity_raw.svg','-dsvg')
  endif

endfor
